<!--
/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
-->
<#-- @ftlvariable name="data" type="com.khronosync.decurse.IndexData" -->
<html>
<head>
  <title>Ubuntu</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
</head>
<body>
<div>
  <h1>Ubuntu</h1>
  <ul>
    <li><strong>Homepage:</strong> <a href="https://www.ubuntu.com/">https://www.ubuntu.com/</a></li>
    <li><strong>Description:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec quis leo efficitur, aliquet massa in, interdum lectus. Phasellus facilisis placerat
      magna vel aliquet. Integer risus neque, sollicitudin eget mauris ac, iaculis ultrices elit.
      Curabitur lobortis faucibus justo sit amet vehicula. Suspendisse dui mauris, dictum sit amet
      enim eget, mattis posuere felis. Phasellus sagittis consequat justo, vel varius sapien
      molestie vulputate. Duis ante urna, accumsan mattis iaculis vel, vehicula id magna. </li>
    <li><strong>Tags:</strong> <code>Workstation</code> <code>Server</code> <code>IoT</code>
      <code>Cloud</code> </li>
  </ul>
  <h3>Ratings</h3>
  <ul>
    <li>
      Overall:
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star-half-alt"></i>
    </li>
    <li>
      Workstation:
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="far fa-star"></i>
    </li>
    <li>
      Server:
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
    </li>
    <li>
      IoT:
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star"></i>
      <i class="fas fa-star-half-alt"></i>
      <i class="far fa-star"></i>
    </li>
  </ul>
</div>
</body>
</html>
