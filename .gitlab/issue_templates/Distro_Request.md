Name:  

(The project's preferred name.)


Description:  

(A brief description of the goals of the project and what they offer users. What differentiates them from other projects.)


Description Source:

(URL of the above description, if it is taken from another source. (i.e, the project's homepage, wikipedia, distrowatch, etc...))


Homepage:

(URL to the project's homepage.)


Horizontal Logo:

(This is the image that shows up on the Linux Delta homepage. Generally contains a logo/symbol with the projects name to the right or left. File type preference: .svg -> medium sized .png -> large sized .png)


Vertical Logo:

(This is the image that shows up on the project's details page. Generally contains a logo/symbol with the projects name above or below. Can sometimes just be the logo/symbol. File type preference: .svg -> medium sized .png -> large sized .png)


Logo BG Color (optional):

(The hex color code of the preferred background color for the logos. Example: #ffffff)

/label ~new-distro
/cc @Kpovoc
