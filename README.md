# DECURSE  

`D`istro  
`E`limination  
`C`hallenge  
`R`ating  
`S`ystem  
`E`xtraordinaire  

A web application meant to provide a system for users to rate and comment on their favorite Linux 
distributions. Derived from the "Distro Elimination Challenge", a former segment on the 
[Ask Noah Show](https://asknoahshow.com) podcast.